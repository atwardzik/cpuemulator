//
// Created by Artur Twardzik on 1/12/2021.
//

#ifndef CPUEMULATOR_DEBUG_H
#define CPUEMULATOR_DEBUG_H

#include <sstream>
#include <string>
#include <iostream>

namespace debug {
    std::string dec2hex(int32_t number, bool hex_prefix = true);

    const char *format_exception_message(std::string &&text, const uint32_t &address);

    const char *format_exception_message(std::string &text, const std::string &insert);
}


#endif //CPUEMULATOR_DEBUG_H
