//
// Created by Artur Twardzik on 3/28/2021.
//

#include "argument_parser.h"

Parser::Parser(int argc, char **argv) {
    std::vector<std::string> parameters(argv + 1, argv + argc);
    auto it = parameters.begin();

    while (it != parameters.end()) {
        if (it->starts_with("--")) {
            optional_arguments.push_back(*it);
        }
        else if (it->starts_with('-')) {
            auto parameter_name = *it;
            ++it;
            auto value = *it;

            arguments.insert(std::make_pair(parameter_name, value));
        }
        else {
            source_file = *it;
        }
        ++it;
    }
}

std::string Parser::get_value(const std::string &parameter_name) const {
    if (!arguments.contains(parameter_name)) {
        std::string message = "Parameter: % not recognised";
        throw std::logic_error(debug::format_exception_message(message, parameter_name));
    }

    return arguments.at(parameter_name);
}

bool Parser::is_parameter_set(const std::string &parameter) const {
    if (arguments.contains(parameter) ||
        std::find(optional_arguments.begin(),
                  optional_arguments.end(), parameter) != std::end(optional_arguments)) {
        return true;
    }
    return false;
}
