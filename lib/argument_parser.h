//
// Created by Artur Twardzik on 3/28/2021.
//

#ifndef DEBUGGER_ARGUMENT_PARSER_H
#define DEBUGGER_ARGUMENT_PARSER_H

#include <iostream>
#include <map>
#include <vector>
#include <string>
#include "debug.h"

class Parser {
private:
    std::map<std::string, std::string> arguments;

    std::vector<std::string> optional_arguments;

    std::string source_file{};
public:

    explicit Parser(int argc, char **argv);

    std::string get_value(const std::string &parameter_name) const;

    std::vector<std::string> get_optional_parameters() const { return optional_arguments; }

    bool is_parameter_set(const std::string &parameter) const;

    std::string get_source_file() const { return source_file; }
};

#endif //DEBUGGER_ARGUMENT_PARSER_H
