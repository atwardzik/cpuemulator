//
// Created by Artur Twardzik on 1/14/2021.
//

#include "debug.h"


//! \description Function converts given number to hexadecimal.
//! \param number Number to convert.
//! \param hex_prefix Option which allows removing <i>0x</i> prefix.
//! \return String containing hexadecimal value.
inline std::string debug::dec2hex(int32_t number, bool hex_prefix) {
    std::stringstream str;
    str << std::hex << std::uppercase;
    if (hex_prefix) str << "0x" << number;
    else str << number;

    return str.str();
}


//! \description Function insert <i>uint32_t</i> value at given place.
//!
//! \param text String containing message to format. Location where address should be added
//!     is marked with <i>%</i> (percent) sign. If <i>%</i> not found the value is inserted
//!     at the end.
//!     <br/><b> Note that the function takes rvalue reference. Use std::forward
//!     if lvalue value is passed.
//!
//! \param address Value to insert.
//!
//! \return Const formatted string aka <i>const char*</i>
const char *debug::format_exception_message(std::string &&text, const uint32_t &address) {
    auto position = text.find('%');
    if (position != std::string::npos) text.replace(position, 1, dec2hex(address));
    else text += " " + dec2hex(address);

    return text.c_str();
}

const char *debug::format_exception_message(std::string &text, const std::string &insert) {
    auto position = text.find('%');
    if (position != std::string::npos) text.replace(position, 1, insert);

    return text.c_str();
}
