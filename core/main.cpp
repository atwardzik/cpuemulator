#include "src/bus.h"
#include "../lib/argument_parser.h"

/* PARAMETERS:
 * -s RAM size, default 65535 bytes, max 4 GB
 * */

int main(int argc, char **argv) {
    std::string source_file{};
    uint32_t memory_size = 0xffff;
#ifndef DEBUG
    if (argc < 2) {
        std::cerr << "fatal error: No input files\n";
        return -1;
    }

    Parser parser(argc, argv);
    source_file = parser.get_source_file();
    if (parser.is_parameter_set("-s")) {
        memory_size = std::stoi(parser.get_value("-s"));
    }
#else
    source_file = "source.mc";
#endif

    try {
        bus::bus bus;
        bus.address_memory(memory_size);
        bus.load_source_code(source_file);
        while (bus::bus::next_instruction) {
            bus.transfer_instruction();
            bus.execute_instruction();
        }
    }
    catch (std::exception &e) {
        std::cout << e.what();
    }
    return 0;
}