//
// Created by Artur Twardzik on 1/2/2021.
//
#include "cpu.h"

void cpu::control_unit::system_interrupt(const uint8_t &number, ram::RAM &RAM) {
    if (number == 0x80) {
        switch (reg(0)) {
            case 0x01: //sys_exit
                sys_exit = true;
                std::cout << "\n\nProcess finished with exit code "
                          << general_purpose.at(1) << std::endl;
                break;
            case 0x03: //sys_read
            {
                const uint32_t where = reg(1);
                uint32_t address = reg(2);
                const uint32_t number_of_bytes = reg(3);

                std::string value{};
                std::getline(std::cin, value);

                std::vector<uint8_t> ret;
                for (size_t c = 0; c < number_of_bytes; ++c) {
                    if (c < value.length()) ret.push_back(value.at(c));
                    else break;
                }

                if (where == 0) { //stdin
                    for (auto byte : ret) {
                        RAM.assign_value(address, 1, byte);
                        ++address;
                    }
                }
            }
                break;
            case 0x04: //sys_write
            {
                /*
                 * This function writes specified value to specified location:
                 * R1 specifies location (stdout, file)
                 * R2 specifies begin address of variable containing the string
                 * R3 specifies number of bytes to print
                 * */

                const uint32_t where = reg(1);
                uint32_t address = reg(2);
                const uint32_t number_of_bytes = reg(3);

                std::vector<uint8_t> str;
                for (size_t byte = 0; byte < number_of_bytes; ++byte) {
                    str.push_back(RAM.get_value(address, 1));
                    ++address;
                }

                if (where == 1) { //stdout
                    for (size_t byte = 0; byte < number_of_bytes; ++byte) {
                        if (str.at(byte) != 0) std::cout << str.at(byte);
                    }
                }
            }
                break;
        }
    }
    else if (number == 0x03) {
        tf = true;
    }
    else {
        throw std::logic_error(debug::format_exception_message(
                "Not found interrupt with code: %.",
                number
        ));
    }
}

uint32_t cpu::control_unit::get_eip() const {
    return eip;
}

int32_t &cpu::control_unit::reg(const uint32_t &reg) {
    if (reg < 0x10) return general_purpose.at(reg);
    else return special_purpose_register(reg);
}

int32_t &cpu::control_unit::special_purpose_register(const uint32_t &reg) {
    if (reg > 0x14) {
        throw std::out_of_range(
                debug::format_exception_message(
                        "Not found register: ",
                        reg
                )
        );
    }

    if (reg == 0x10) return esp;
    else if (reg == 0x11) return ebp;
    else if (reg == 0x12) return esi;
    else if (reg == 0x13) return edi;
    else return eip;
}

int32_t cpu::control_unit::convert_to_single_value(std::vector<uint32_t> &bytes_to_convert,
                                                   std::vector<uint32_t>::iterator first,
                                                   std::vector<uint32_t>::iterator last) const {
    /*
     * Method convert every single byte (stored as decimal integer) to hexadecimal equivalent.
     * It's in order to convert it to proper integer value. Why? Follow the example:
     *
     * Let's suppose we write 4 bytes as parameter in machine code (1),
     * then it is stored in our RAM module as 4 bytes, but integers are automatically
     * stored in their decimal version. So if we call our CPU module it gets vector
     * of 4 bytes stored as DECIMAL INTEGERS (2)! To reverse automatic conversion
     * we convert every single byte back to hexadecimal version and store it in a string.
     * Then using std::stoll (to get full range i.e. up to 0xffffffff) we convert this string
     * to integer, pointing out, that the base is 16.
     *
     * (1) 00  AB 1C 01     | HEX
     * (2)  0 171 28  1     | DEC
     * */
    auto dec2hex = [](const uint32_t &number) {
        std::stringstream str;
        str << std::hex << std::uppercase;
        if (number < 16) str << 0;
        str << number;

        return str.str();
    };

    std::string bytes{};
    for (auto byte = first; byte != last; ++byte) {
        bytes += dec2hex(*byte);
    }

    return std::stoll(bytes, nullptr, 16);
}

uint32_t cpu::control_unit::handle_number(uint8_t pos_start, uint8_t pos_end) {
    if (pos_end == 0) pos_end = parameters.size();

    auto it_begin = std::next(parameters.begin(), pos_start);
    auto it_end = std::next(parameters.begin(), pos_end);

    uint32_t converted_number = convert_to_single_value(parameters, it_begin, it_end);

    return converted_number;
}

void cpu::control_unit::handle_jump(const std::function<bool()> &fPtr) {
    if (fPtr()) {
        eip = handle_number();
    }
    else eip += 5; //4 for parameter + 1 for opcode
}

void cpu::control_unit::handle_ret(ram::RAM &RAM, const uint32_t &amount_to_pop) {
    try {
        eip = RAM.pop(); //function return address
        esp += 4;

        for (size_t i = 0; i < amount_to_pop; ++i) {
            RAM.pop();
            esp += 4;
        }
    }
    catch (std::out_of_range &e) {
        throw std::runtime_error("Segmentation fault");
    }
}

void cpu::control_unit::handle_cmp(const int32_t &left_operand, const int32_t &right_operand) {
    /*
     * CF - 1 if unsigned op2 > unsigned op1
     * OF - 1 if sign bit of OP1 != sign bit of result
     * SF - 1 if MSB (most significant bit; aka sign bit) of result = 1
     * ZF - 1 if Result = 0 (i.e. op1=op2)
     * */

    auto result = left_operand - right_operand;
    uint32_t u_op1 = left_operand;
    uint32_t u_op2 = right_operand;

    if (u_op2 > u_op1) cf = true;
    else cf = false;


    if (std::signbit(static_cast<float>(left_operand)) != std::signbit(static_cast<float>(result))) of = true;
    else of = false;


    if (result < 0) sf = true;
    else sf = false;


    if (result == 0) zf = true;
    else zf = false;
}

void cpu::control_unit::handle_test(const int32_t &left_operand, const int32_t &right_operand) {
    /*
     * The TEST instruction perform AND operation on specified values without saving the result
     * to the register and instead setting the SF and ZF.
     * ZF is set only if both operands are equal to zero
     * SF is set on the basis of the most significant bit after making AND*/

    auto result = left_operand & right_operand;

    if (result == 0) zf = true;
    else zf = false;

    if (std::signbit(static_cast<float>(result))) sf = true;
    else sf = false;
}

void cpu::control_unit::handle_arithmetic(const uint8_t &opcode, bool number) {
    auto destination = parameters.at(0);

    if (parameters.size() > 1) {
        int32_t source = handle_number(1);
        if (!number) source = reg(source);

        reg(destination) = this->compute_arithmetic(opcode, reg(destination), source);
    }
    else {
        reg(destination) = this->compute_arithmetic(opcode, reg(destination));
    }
}

void cpu::control_unit::insert_or_assign(const uint32_t &where,
                                         const uint32_t &what,
                                         const uint8_t &number_of_bytes,
                                         ram::RAM &RAM) {
    if (!RAM.address_already_exist(where)) {
        RAM.allocate_value(where, number_of_bytes, what);
    }
    else {
        RAM.assign_value(where, number_of_bytes, what);
    }
}

void cpu::control_unit::set_esp(uint32_t ram_size) {
    esp = ram_size;
}

//! This method sets opcode for specific instruction and assigns size of parameters to read.
void cpu::control_unit::set_instruction(const uint8_t &opcode) {
    instruction = opcode;
    //opcodes are sorted ascending by number of parameters
    if (opcode < 0x05) instruction_parameters_size = 0;
    else if (opcode <= 0x10) instruction_parameters_size = 1;
    else if (opcode <= 0x20) instruction_parameters_size = 4;
    else if (opcode <= 0x30 || (opcode >= 0x61 && opcode < 0x63)) instruction_parameters_size = 2;
    else if (opcode <= 0x45 || opcode >= 0x63) instruction_parameters_size = 5;
    else if (opcode <= 0x55) instruction_parameters_size = 8;
    else {
        throw std::logic_error(
                debug::format_exception_message(
                        "Not found instruction with opcode: %.",
                        opcode
                )
        );
    }

}

uint16_t cpu::control_unit::get_parameters_size() const {
    return instruction_parameters_size;
}

//! This method sets given bytes as parameters to instruction.
//! \param params Vector of bytes representing instruction parameters.
void cpu::control_unit::set_instruction_parameters(std::vector<uint32_t> params) {
    parameters.clear();
    std::copy(params.begin(), params.end(), std::back_inserter(parameters));
}

//! This method executes requested instruction.
//! \param RAM Reference to instance of Random Access Memory.
void cpu::control_unit::execute(ram::RAM &RAM) {
    switch (instruction) {
        case 0x00: //NOP
            eip += 1;
            break;
        case 0x01: //POP
            RAM.pop();
            eip += 1;
            esp += 4;
            break;
        case 0x02: //RET
            handle_ret(RAM);
            break;
        case 0x05: //INC [REG]
            handle_arithmetic(0x04);
            eip += 2;
            break;
        case 0x06: //DEC [REG]
            handle_arithmetic(0x05);
            eip += 2;
            break;
        case 0x07: //POP [REG]
        {
            uint32_t destination_register = parameters.at(0);
            reg(destination_register) = RAM.pop();

            esp += 4;
            eip += 2;
        }
            break;
        case 0x08: //PUSH [REG]
        {
            uint32_t source_register = parameters.at(0);
            RAM.push(reg(source_register));
            esp -= 4;

            eip += 2;
        }
            break;
        case 0x09: //NOT [REG]
            handle_arithmetic(0x09);
            eip += 2;
            break;
        case 0x0A: //RET [REG]
        {
            auto amount_to_pop = reg(parameters.at(0));
            handle_ret(RAM, amount_to_pop);
        }
            break;
        case 0x10: //INT [VALUE]
            system_interrupt(parameters.at(0), RAM);
            eip += 2;
            break;
        case 0x11: //CALL [LABEL]
        {
            auto function_pointer = handle_number();
            eip += 5; //push address of instruction which is located after call
            RAM.push(eip);
            esp -= 4;
            eip = function_pointer;
        }
            break;
        case 0x12: //JMP [LABEL]
        {
            auto location = handle_number();
            eip = location;
        }
            break;
        case 0x13: //JE [LABEL]
            handle_jump([this]() { return (zf ? true : false); });
            break;
        case 0x14: //JG [LABEL]
            handle_jump([this]() { return ((sf == of) && (zf == 0)) ? true : false; });
            break;
        case 0x15: //JGE [LABEL]
            handle_jump([this]() { return ((sf == of) || zf) ? true : false; });
            break;
        case 0x16: //JL [LABEL]
            handle_jump([this]() { return (sf != of) ? true : false; });
            break;
        case 0x17: //JLE [LABEL]
            handle_jump([this]() { return ((sf != of) || zf) ? true : false; });
            break;
        case 0x18: //JNE [LABEL]
            handle_jump([this]() { return (!zf ? true : false); });
            break;
        case 0x19: //PUSH [VALUE]
        {
            int32_t source_value = handle_number();
            RAM.push(source_value);

            esp -= 4;
            eip += 5;
        }
            break;
        case 0x20: //RET [VALUE]
        {
            auto amount_to_pop = handle_number();
            handle_ret(RAM, amount_to_pop);
        }
            break;
        case 0x21: //MOV [REG], [REG]
        {
            uint32_t destination_register = parameters.at(0);
            uint32_t source_register = parameters.at(1);

            reg(destination_register) = reg(source_register);

            eip += 3;
        }
            break;
        case 0x22: //CMP [REG], [REG]
        {
            int32_t left_operand = reg(parameters.at(0));
            int32_t right_operand = reg(parameters.at(1));

            handle_cmp(left_operand, right_operand);

            eip += 3;
        }
            break;
        case 0x23: //MUL [REG], [REG]
            handle_arithmetic(0x00);
            eip += 3;
            break;
        case 0x24: //DIV [REG], [REG]
            handle_arithmetic(0x01);
            eip += 3;
            break;
        case 0x25: //ADD [REG], [REG]
            handle_arithmetic(0x02);
            eip += 3;
            break;
        case 0x26: //SUB [REG], [REG]
            handle_arithmetic(0x03);
            eip += 3;
            break;
        case 0x27: //XOR [REG], [REG]
            handle_arithmetic(0x08);
            eip += 3;
            break;
        case 0x28: //AND [REG], [REG]
            handle_arithmetic(0x06);
            eip += 3;
            break;
        case 0x29: //OR [REG], [REG]
            handle_arithmetic(0x07);
            eip += 3;
            break;
        case 0x2A: //TEST [REG], [REG]
        {
            int32_t left_operand = reg(parameters.at(0));
            int32_t right_operand = reg(parameters.at(1));

            handle_test(left_operand, right_operand);

            eip += 3;
        }
            break;
        case 0x2B: //MOV [REG], BYTE PTR[REG] - dereference value
        {
            uint8_t destination_register = parameters.at(0);
            uint8_t source_reg_with_address = parameters.at(1);

            reg(destination_register) = RAM.get_value(reg(source_reg_with_address), 1);

            eip += 3;
        }
            break;
        case 0x2C: //MOV [REG], WORD PTR[REG]
        {
            uint8_t destination_register = parameters.at(0);
            uint8_t source_reg_with_address = parameters.at(1);

            reg(destination_register) = RAM.get_value(reg(source_reg_with_address), 2);

            eip += 3;
        }
            break;
        case 0x2D: //MOV [REG], DWORD PTR[REG]
        {
            uint8_t destination_register = parameters.at(0);
            uint8_t source_reg_with_address = parameters.at(1);

            reg(destination_register) = RAM.get_value(reg(source_reg_with_address), 4);

            eip += 3;
        }
            break;
        case 0x2E: //MOV BYTE PTR[REG], [REG]
        {
            uint32_t destination = reg(parameters.at(0));
            uint32_t source = reg(parameters.at(1));

            this->insert_or_assign(destination, source, 1, RAM);

            eip += 3;
        }
            break;
        case 0x2F: //MOV WORD PTR[REG], [REG]
        {
            uint32_t destination = reg(parameters.at(0));
            uint32_t source = reg(parameters.at(1));

            this->insert_or_assign(destination, source, 2, RAM);

            eip += 3;
        }
            break;
        case 0x30: //MOV DWORD PTR[REG], [REG]
        {
            uint32_t destination = reg(parameters.at(0));
            uint32_t source = reg(parameters.at(1));

            this->insert_or_assign(destination, source, 4, RAM);

            eip += 3;
        }
            break;
        case 0x31: //MOV [REG], [VALUE]
        {
            uint32_t destination_register = parameters.at(0);
            int32_t source_value = handle_number(1); //using int32_t, 2's complement provided

            reg(destination_register) = source_value;

            eip += 6;
        }
            break;
        case 0x32: //CMP [REG], [VALUE]
        {
            int32_t left_operand = reg(parameters.at(0));
            int32_t right_operand = handle_number(1);

            handle_cmp(left_operand, right_operand);

            eip += 6;
        }
            break;
        case 0x33: //MUL [REG], [VALUE]
            handle_arithmetic(0x00, true);
            eip += 6;
            break;
        case 0x34: //DIV [REG], [VALUE]
            handle_arithmetic(0x01, true);
            eip += 6;
            break;
        case 0x35: //ADD [REG], [VALUE]
            handle_arithmetic(0x02, true);
            eip += 6;
            break;
        case 0x36: //SUB [REG], [VALUE]
            handle_arithmetic(0x03, true);
            eip += 6;
            break;
        case 0x37: //XOR [REG], [VALUE]
            handle_arithmetic(0x08, true);
            eip += 6;
            break;
        case 0x38: //AND [REG], [VALUE]
            handle_arithmetic(0x06, true);
            eip += 6;
            break;
        case 0x39: //OR [REG], [VALUE]
            handle_arithmetic(0x07, true);
            eip += 6;
            break;
        case 0x3A: //TEST [REG], [VALUE]
        {
            int32_t left_operand = reg(parameters.at(0));
            int32_t right_operand = handle_number(1);

            handle_test(left_operand, right_operand);

            eip += 6;
        }
            break;
        case 0x3B: //MOV REG, BYTE PTR[ADDRESS]
        {
            uint8_t destination_register = parameters.at(0);
            uint32_t source_location = handle_number(1);

            reg(destination_register) = RAM.get_value(source_location, 1);

            eip += 6;
        }
            break;
        case 0x3C: //MOV REG, WORD PTR[ADDRESS]
        {
            uint8_t destination_register = parameters.at(0);
            uint32_t source_location = handle_number(1);

            reg(destination_register) = RAM.get_value(source_location, 2);

            eip += 6;
        }
            break;
        case 0x3D: //MOV REG, DWORD PTR[ADDRESS]
        {
            uint8_t destination_register = parameters.at(0);
            uint32_t source_location = handle_number(1);

            reg(destination_register) = RAM.get_value(source_location, 4);

            eip += 6;
        }
            break;
        case 0x3E: //MOV BYTE PTR[REG], [VALUE]
        {
            uint32_t destination = reg(parameters.at(0));
            auto value = handle_number(1);

            this->insert_or_assign(destination, value, 1, RAM);

            eip += 6;
        }
            break;
        case 0x3F: //MOV WORD PTR[REG], [VALUE]
        {
            uint32_t destination = reg(parameters.at(0));
            auto value = handle_number(1);

            this->insert_or_assign(destination, value, 2, RAM);

            eip += 6;
        }
            break;
        case 0x40: //MOV DWORD PTR[REG], [VALUE]
        {
            uint32_t destination = reg(parameters.at(0));
            auto value = handle_number(1);

            this->insert_or_assign(destination, value, 4, RAM);

            eip += 6;
        }
            break;
        case 0x43: //MOV BYTE PTR [ADDRESS], [REG]
        {
            uint32_t destination_address = handle_number(0, 4);
            uint32_t source_register = reg(parameters.back());

            this->insert_or_assign(destination_address, source_register, 1, RAM);

            eip += 6;
        }
            break;
        case 0x44: //MOV WORD PTR [ADDRESS], [REG]
        {
            uint32_t destination_address = handle_number(0, 4);
            uint32_t source_register = reg(parameters.back());

            this->insert_or_assign(destination_address, source_register, 2, RAM);

            eip += 6;
        }
            break;
        case 0x45: //MOV DWORD PTR [ADDRESS], [REG]
        {
            uint32_t destination_address = handle_number(0, 4);
            uint32_t source_register = reg(parameters.back());

            this->insert_or_assign(destination_address, source_register, 4, RAM);

            eip += 6;
        }
            break;
        case 0x52: //CMP [VALUE], [VALUE]
        {
            int32_t left_operand = handle_number(0, 4);
            int32_t right_operand = handle_number(4);

            handle_cmp(left_operand, right_operand);

            eip += 9;
        }
            break;
        case 0x53: //MOV BYTE PTR[ADDRESS], [VALUE]
        {
            auto destination_address = handle_number(0, 4);
            auto value = handle_number(4);

            this->insert_or_assign(destination_address, value, 1, RAM);

            eip += 9;
        }
            break;
        case 0x54: //MOV WORD PTR[ADDRESS], [VALUE]
        {
            auto destination_address = handle_number(0, 4);
            auto value = handle_number(4);

            this->insert_or_assign(destination_address, value, 2, RAM);

            eip += 9;
        }
            break;
        case 0x55: //MOV DWORD PTR[ADDRESS], [VALUE]
        {
            auto destination_address = handle_number(0, 4);
            auto value = handle_number(4);

            this->insert_or_assign(destination_address, value, 4, RAM);

            eip += 9;
        }
            break;
        case 0x61: //SHL [REG], [REG]
            handle_arithmetic(0x0A);
            eip += 3;
            break;
        case 0x62: //SHR [REG], [REG]
            handle_arithmetic(0x0B);
            eip += 3;
            break;
        case 0x71: //SHL [REG], [VALUE]
            handle_arithmetic(0x0A, true);
            eip += 6;
            break;
        case 0x72: //SHR [REG], [VALUE]
            handle_arithmetic(0x0B, true);
            eip += 6;
            break;
    }

    RAM.check_stack(esp);

#ifdef DEBUG
    std::cout << "\nINSTRUCTION: " << static_cast<int>(instruction);
    std::cout << "\nGENERAL PURPOSES: ";
    for (auto element : general_purpose) {
        std::cout << element << " ";
    }
    std::cout << "\nSPECIAL PURPOSES: ";
    for (size_t i = 0x10; i < 0x15; ++i) {
        std::cout << special_purpose_register(i) << " ";
    }
    std::cout << "\nFLAGS: "
              << "ZF: " << zf
              << " | CF: " << cf
              << " | OF: " << of
              << " | SF: " << sf;
    std::cout << "\n---------------------\n";
#endif
}

void cpu::control_unit::print_registers() const {
    for (size_t i = 0; i < general_purpose.size(); ++i) {
        std::cout << "R" << i << " : 0x" << std::hex << general_purpose.at(i) << std::dec << std::endl;
    }
    std::cout << std::hex
              << "ESP : 0x" << esp
              << "\nEBP : 0x" << ebp
              << "\nESI : 0x" << esi
              << "\nEDI : 0x" << edi
              << "\nEIP : 0x" << eip
              << std::dec << std::endl;
}
